#!/bin/bash
set -e
set -x

docker build . -t worker_image
docker rm worker_service || true
docker run -it -p 3200:80 --name worker_service worker_image
