console.log('hello from worker')

let express = require('express')
let fetch = require('node-fetch')

let app = express()

app.use((req, res, next) => {
  console.log(req.method, req.url)
  next()
})

app.get('/', (req, res) => {
  res.end(/* html */`
  <p>hello from worker server</p>
  <a href="/report">report to master</a>
  `)
})

app.get('/report', (req, res) => {
  fetch('http://master_service/job')
    .then(res => res.json())
    .then(data => res.json(data))
})

app.listen(80, () => {
  console.log('listening on http://localhost:80')
})
