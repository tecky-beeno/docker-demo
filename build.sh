#!/bin/bash
set -e
set -o pipefail
set -x

cd clock
pnpm i
npm run build
docker build . -t clock_image