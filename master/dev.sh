#!/bin/bash
set -e
set -x

docker build . -t master_image
docker rm master_service || true
docker run -it -p 3000:80 --name master_service master_image
