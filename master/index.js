console.log('hello from master')

let express = require('express')

let app = express()

app.use((req, res, next) => {
  console.log(req.method, req.url)
  next()
})

app.get('/', (req, res) => {
  res.end('hello from master server')
})

app.get('/job', (req, res) => {
  res.json('no job')
})

app.listen(80, () => {
  console.log('listening on http://localhost:80')
})
